﻿namespace ExtensionMethods;

public static class ObjectExtensions
{
    #region Public Methods

    /// <summary>
    /// Выполняет обработку отображения значения в столбце.
    /// </summary>
    /// <param name="inp">Обрабатываемое значение</param>
    /// <param name="len">Ширина столбца</param>
    /// <returns>Укороченное значение или значение с добавленными пробелами</returns>
    public static string Show(this object? inp, int len)
    {
        string space = "";
        if (inp == null)
        {
            for (int i = 0; i < len; i++)
                space += " ";
            return space;
        }
        else
        {
            string? input = inp.ToString();
            if (string.IsNullOrEmpty(input) || input.Length <= len)
            {
                for (int i = 0; i < len - input?.Length; i++)
                    space += " ";
                return input + space;
            }
            return string.Concat(input.AsSpan(0, len - 3), "...");
        }
    }

    #endregion
}