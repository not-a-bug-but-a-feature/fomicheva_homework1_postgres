﻿namespace homework1.Models;

/// <summary>
/// Модель региона
/// </summary>
public sealed class Region
{
    #region Public Properties

    /// <summary>
    /// Идентификатор
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Название региона на английском
    /// </summary>
    [Required]
    [StringLength(50)]
    public string RegionNameEn { get; set; } = null!;

    /// <summary>
    /// Название региона на русском
    /// </summary>
    [Required]
    [StringLength(50)]
    public string RegionNameRu { get; set; } = null!;

    #endregion

    #region Public Methods

    public override string ToString() => $"{Id.Show(Tables.ColumnRegions[0].Length)} | " +
        $"{RegionNameEn.Show(Tables.ColumnRegions[1].Length)} | " +
        $"{RegionNameRu.Show(Tables.ColumnRegions[2].Length)} |";

    #endregion
}