﻿namespace homework1.Models;

public sealed class CommMethod
{
    #region Public Properties

    /// <summary>
    /// Идентификатор
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Способ связи
    /// </summary>
    [Required]
    [StringLength(50)]
    public string Method { get; set; } = null!;

    #endregion

    #region Public Methods

    public override string ToString() => $"{Id.Show(Tables.ColumnCommMethods[0].Length)} | " +
        $"{Method.Show(Tables.ColumnCommMethods[1].Length)} |";

    #endregion
}