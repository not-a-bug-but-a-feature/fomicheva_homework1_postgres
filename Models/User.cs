﻿namespace homework1.Models;

/// <summary>
/// Модель пользователя
/// </summary>
public sealed class User
{
    #region Public Properties

    /// <summary>
    /// Идентификатор
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Фамилия
    /// </summary>
    [Required]
    [Display(Name = "Last Name")]
    [StringLength(50)]
    public string LastName { get; set; } = null!;

    /// <summary>
    /// Имя
    /// </summary>
    [Required]
    [Display(Name = "First Name")]
    [StringLength(50)]
    public string FirstName { get; set; } = null!;

    /// <summary>
    /// Отчество
    /// </summary>
    [Display(Name = "Middle Name")]
    [StringLength(50)]
    public string? MiddleName { get; set; }

    /// <summary>
    /// Электронная почта
    /// </summary>
    [Required]
    public string Email { get; set; } = null!;

    [Required]
    /// <summary>
    /// Телефон
    /// </summary>
    public string Phone { get; set; } = null!;

    [Required]
    /// <summary>
    /// Идентификатор региона. Внешний ключ
    /// </summary>
    public int RegionId { get; set; }

    /// <summary>
    /// Навигационное свойство
    /// </summary>
    public Region Region { get; set; } = null!;

    /// <summary>
    /// Рейтинг
    /// </summary>
    public double? Rating { get; set; }

    #endregion

    #region Public Methods

    public override string ToString() => $"{Id.Show(Tables.ColumnUsers[0].Length)} | {LastName.Show(Tables.ColumnUsers[1].Length)} | " +
        $"{FirstName.Show(Tables.ColumnUsers[2].Length)} | {MiddleName.Show(Tables.ColumnUsers[3].Length)} " +
        $"| {Email.Show(Tables.ColumnUsers[4].Length)} | {Phone.Show(Tables.ColumnUsers[5].Length)} | " +
        $"{RegionId.Show(Tables.ColumnUsers[6].Length)} | {Rating.Show(Tables.ColumnUsers[7].Length)} |";

    #endregion
}