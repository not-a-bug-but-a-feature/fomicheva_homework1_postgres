﻿namespace homework1.Models;

/// <summary>
/// Модель объявления
/// </summary>
public sealed class Ad
{
    #region Public Properties

    /// <summary>
    /// Идентификатор
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Идентификатор пользователя. Внешний ключ
    /// </summary>
    [Required]
    public int UserId { get; set; }

    /// <summary>
    /// Навигационное свойство
    /// </summary>
    public User User { get; set; } = null!;

    /// <summary>
    /// Идентификатор способа связи. Внешний ключ
    /// </summary>
    [Required]
    public int CommMethodId { get; set; }

    /// <summary>
    /// Навигационное свойство
    /// </summary>
    public CommMethod СommMethod { get; set; } = null!;

    /// <summary>
    /// Идентификатор категории. Внешний ключ
    /// </summary>
    [Required]
    public int CategoryId { get; set; }

    /// <summary>
    /// Навигационное свойство
    /// </summary>
    public Category Category { get; set; } = null!;

    /// <summary>
    /// Текст объявления
    /// </summary>
    [StringLength(88)]
    [Required]
    public string Text { get; set; } = null!;

    /// <summary>
    /// Дата размещения объявления
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    /// Цена
    /// </summary>
    [Required]
    public decimal Price { get; set; }

    /// <summary>
    /// Название объявления
    /// </summary>
    [Required]
    [StringLength(50)]
    public string Name { get; set; } = null!;

    #endregion

    #region Public Methods

    public override string ToString() => $"{Id.Show(Tables.ColumnAds[0].Length)} | {UserId.Show(Tables.ColumnAds[1].Length)} | " +
        $"{CommMethodId.Show(Tables.ColumnAds[2].Length)} | {CategoryId.Show(Tables.ColumnAds[3].Length)} " +
        $"| {Text.Show(Tables.ColumnAds[4].Length)} | {Date.ToShortDateString().Show(Tables.ColumnAds[5].Length)} | " +
        $"{Price.Show(Tables.ColumnAds[6].Length)} | {Name.Show(Tables.ColumnAds[7].Length)} |";
    
    #endregion
}