﻿namespace homework1.Models;

/// <summary>
/// Модель категории
/// </summary>
public sealed class Category
{
    #region Public Properties

    /// <summary>
    /// Идентификатор
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Название категории на английском
    /// </summary>
    [Required]
    [StringLength(50)]
    public string CategoryNameEn { get; set; } = null!;

    /// <summary>
    /// Название категории на русском
    /// </summary>
    [Required]
    [StringLength(50)]
    public string CategoryNameRu { get; set; } = null!;

    #endregion

    #region Public Methods

    public override string ToString() => $"{Id.Show(Tables.ColumnCategories[0].Length)} | " +
        $"{CategoryNameEn.Show(Tables.ColumnCategories[1].Length)} | " +
        $"{CategoryNameRu.Show(Tables.ColumnCategories[2].Length)} |";

    #endregion
}