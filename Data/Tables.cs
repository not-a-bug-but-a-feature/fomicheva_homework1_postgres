﻿namespace homework1.Data;

public static class Tables
{

    #region Public Fields

    public const string TableRegions = "РЕГИОНЫ", TableUsers = "ПОЛЬЗОВАТЕЛИ", TableAds = "ОБЪЯВЛЕНИЯ", TableCategories = "КАТЕГОРИИ", TableCommMethods = "СПОСОБЫ СВЯЗИ";
    public static List<string> ColumnRegions = new() { "ID", "Название региона на английском", "Название региона на русском" };
    public static List<string> ColumnCommMethods = new() { "ID", "Способ связи" };
    public static List<string> ColumnCategories = new() { "ID", "Название категории на английском", "Название категории на русском" };
    public static List<string> ColumnUsers = new() { "ID", "Фамилия", "Имя", "Отчество", "Электронная почта", "Телефон", "ID региона", "Рейтинг" };
    public static List<string> ColumnAds = new() { "ID", "ID пользователя", "ID способа связи", "ID категории", "Текст объявления", "Дата размещения", "Стоимость", "Название объявления" };

    #endregion

    #region Public Methods

    /// <summary>
    /// Формирует заголовок таблицы.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns>Заголовок</returns>
    public static string? ReturnTableHeader<T>()
    {
        var key = typeof(T).Name;
        string phrase;
        switch (key)
        {
            case "Region":
                phrase = $"\n{TableRegions}\n{ColumnRegions[0]} | {ColumnRegions[1]} | {ColumnRegions[2]} |";
                break;
            case "CommMethod":
                phrase = $"\n{TableCommMethods}\n{ColumnCommMethods[0]} | {ColumnCommMethods[1]} |";
                break;
            case "Category":
                phrase = $"\n{TableCategories}\n{ColumnCategories[0]} | {ColumnCategories[1]} | {ColumnCategories[2]} |";
                break;
            case "User":
                phrase = $"\n{TableUsers}\n{ColumnUsers[0]} | {ColumnUsers[1]} | {ColumnUsers[2]} | {ColumnUsers[3]} | {ColumnUsers[4]} | {ColumnUsers[5]} | {ColumnUsers[6]} | {ColumnUsers[7]} |";
                break;
            case "Ad":
                phrase = $"\n{TableAds}\n{ColumnAds[0]} | {ColumnAds[1]} | {ColumnAds[2]} | {ColumnAds[3]} | {ColumnAds[4]} | {ColumnAds[5]} | {ColumnAds[6]} | {ColumnAds[7]} |";
                break;
            default:
                return null;
        }
        return phrase.ToUpper();
    }

    #endregion
}
