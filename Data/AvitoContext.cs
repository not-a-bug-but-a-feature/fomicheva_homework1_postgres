﻿namespace homework1.Data;

public sealed class AvitoContext : DbContext
{
    #region Public Constructors

    public AvitoContext()
    {
        Database.EnsureCreated();
    }

    #endregion

    #region Public Properties

    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Ad> Ads { get; set; } = null!;
    public DbSet<Category> Categories { get; set; } = null!;
    public DbSet<CommMethod> CommMethods { get; set; } = null!;
    public DbSet<Region> Regions { get; set; } = null!;

    #endregion

    #region Protected Methods

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(GetConnString());
    }
    
    #endregion

    #region Private Methods

    /// <summary>
    /// Возващает строку подключения к БД.
    /// </summary>
    /// <returns>Строка подключения</returns>
    private static string GetConnString()
    {
        var confBuilder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

        return confBuilder.Build().GetConnectionString("AvitoContext");
    }

    #endregion
}
