﻿namespace homework1.Data;

public static class DbInitializer
{
    #region Public Methods

    /// <summary>
    /// Наполняет БД исходными данными.
    /// </summary>
    /// <param name="context"></param>
    public static void Initialize(AvitoContext context)
    {
        if (context.Ads.Any())
        {
            return;
        }
        var regions = new Region[]
        {
            new Region{RegionNameRu="Республика Адыгея",RegionNameEn="Republic of Adygea"},
            new Region{RegionNameRu="Республика Башкортостан",RegionNameEn="Republic of Bashkortostan"},
            new Region{RegionNameRu="Республика Бурятия",RegionNameEn="Republic of Buryatia"},
            new Region{RegionNameRu="Республика Алтай",RegionNameEn="Altai Republic"},
            new Region{RegionNameRu="Республика Дагестан",RegionNameEn="Republic of Dagestan"}

        };

        context.Regions.AddRange(regions);
        context.SaveChanges();

        var users = new User[]
        {
            new User{LastName = "Гон",FirstName = "Ю",Email = "user1@example.com",Phone = "88001112233",Rating = 0,RegionId = 1},
            new User{LastName = "Пак",FirstName = "Бон",MiddleName = "Сун",Email = "user2@example.com",Phone = "88001112234",Rating = 1.5,RegionId = 2},
            new User{LastName = "Пак",FirstName = "Ю",MiddleName = "На",Email = "user3@example.com",Phone = "88001112235",Rating = 4.3,RegionId = 5},
            new User{LastName = "Пэ",FirstName = "Хэ",MiddleName = "Сон",Email = "user4@example.com",Phone = "88001112236",RegionId = 3},
            new User{LastName = "Кан",FirstName = "Ми",MiddleName = "На",Email = "user5@example.com",Phone = "88001112237",Rating = 1.5,RegionId = 4}

        };

        context.Users.AddRange(users);
        context.SaveChanges();

        var categories = new Category[]
        {
            new Category{CategoryNameRu="Животные",CategoryNameEn="Animals"},
            new Category{CategoryNameRu="Работа",CategoryNameEn="Job"},
            new Category{CategoryNameRu="Транспорт",CategoryNameEn="Transport"},
            new Category{CategoryNameRu="Недвижимость",CategoryNameEn="Realty"},
            new Category{CategoryNameRu="Электроника",CategoryNameEn="Electronics"}
        };

        context.Categories.AddRange(categories);
        context.SaveChanges();

        var commMethods = new CommMethod[]
        {
            new CommMethod{Method="По телефону и в сообщениях"},
            new CommMethod{Method="По телефону"},
            new CommMethod{Method="В сообщениях"}
        };

        context.CommMethods.AddRange(commMethods);
        context.SaveChanges();

        var ads = new Ad[]
        {
            new Ad{UserId=1,CommMethodId=1,CategoryId=1,Text="Продам котят Мэйн-куна. Возможен торг.",
            Date=DateTime.Parse("2016-09-01").ToUniversalTime(),Price=4000,Name="Котята Мэйн-куна"},
            new Ad{UserId=2,CommMethodId=2,CategoryId=2,Text="Требуется помощник в отдел маркетинга. Необходимо наличие в/о.",
            Date=DateTime.Parse("2019-07-11").ToUniversalTime(),Price=40000,Name="Требуется помощник"},
            new Ad{UserId=3,CommMethodId=3,CategoryId=3,Text="Продам KIA K5 2021 года выпуска в хорошем состоянии.",
            Date=DateTime.Parse("2022-01-02").ToUniversalTime(),Price=2000000,Name="KIA K5 2021"},
            new Ad{UserId=4,CommMethodId=3,CategoryId=4,Text="Продам 4-х комнатную квартиру в центре Горно-Алтайска.",
            Date=DateTime.Parse("2020-11-10").ToUniversalTime(),Price=4000000,Name="Продам квартиру в Горно-Алтайске"},
            new Ad{UserId=5,CommMethodId=2,CategoryId=5,Text="Продам стиральную машину Zanussi в отличном состоянии. Загрузка 6 кг.",
            Date=DateTime.Parse("2020-11-10").ToUniversalTime(),Price=4000000,Name="Стиральная машина Zanussi"}
        };

        context.Ads.AddRange(ads);
        context.SaveChanges();
    }

    #endregion
}