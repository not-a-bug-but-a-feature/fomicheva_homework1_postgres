﻿namespace homework1;
static class Program
{
    #region Private Methods

    static void Main(string[] args)
    {
        var db = new AvitoContext();
        DbInitializer.Initialize(db);
        Menu(db);
    }

    /// <summary>
    /// Меню выбора действия.
    /// </summary>
    /// <param name="context"></param>
    private static void Menu(AvitoContext context)
    {
        Console.Write("\nПривет!");
        while (true)
        {
            Console.WriteLine("\nВыберите действие:\n\t1 - Показать все таблицы" +
            "\n\t2 - Добавить данные в таблицу" +
            "\n\t3 - Выйти");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    PrintTable<Category>(context);
                    PrintTable<Region>(context);
                    PrintTable<CommMethod>(context);
                    PrintTable<User>(context);
                    PrintTable<Ad>(context);
                    break;
                case "2":
                    TableSwitcher(context);
                    break;
                case "3":
                    return;
                default:
                    Console.WriteLine("Неизвестная команда. Попробуйте ввести еще раз");
                    break;
            }
        }
    }

    /// <summary>
    /// Меню выбора таблицы для добавления строки.
    /// </summary>
    /// <param name="context"></param>
    private static void TableSwitcher(AvitoContext context)
    {
        while (true)
        {
            Console.WriteLine("\nВ какую таблицу хотите добавить данные?" +
            "\n\t1 - Категории" +
            "\n\t2 - Регионы" +
            "\n\t3 - Способы связи" +
            "\n\t4 - Пользователи" +
            "\n\t5 - Объявления" +
            "\n\t6 - Вернуться в главное меню");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    AddRow<Category>(context);
                    break;
                case "2":
                    AddRow<Region>(context);
                    break;
                case "3":
                    AddRow<CommMethod>(context);
                    break;
                case "4":
                    AddRow<User>(context);
                    break;
                case "5":
                    AddRow<Ad>(context);
                    break;
                case "6":
                    return;
                default:
                    Console.WriteLine("Неизвестная команда. Попробуйте ввести еще раз");
                    break;
            }
        }
    }
    
    /// <summary>
    /// Получает и выводит таблицу на экран.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="context"></param>
    private static void PrintTable<T>(AvitoContext context) where T : class
    {
        var dbSet = context.Set<T>().ToList();
        Console.WriteLine(Tables.ReturnTableHeader<T>());
        dbSet.ForEach(p => Console.WriteLine(p.ToString()));
    }

    /// <summary>
    /// Добавляет строку в таблицу в зависимости от типа.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="context"></param>
    private static void AddRow<T>(AvitoContext context) where T : class
    {
        try
        {
            bool flag = true;
            var row = new List<string>();
            switch (typeof(T).Name)
            {
                case "Region":
                    AddRowToRegions(context, GetRow(Tables.ColumnRegions, row));
                    break;
                case "CommMethod":
                    AddRowToCommMethods(context, GetRow(Tables.ColumnCommMethods, row));
                    break;
                case "Category":
                    AddRowToCategories(context, GetRow(Tables.ColumnCategories, row));
                    break;
                case "User":
                    flag = AddRowToUsers(context, GetRow(Tables.ColumnUsers, row));
                    break;
                case "Ad":
                    flag = AddRowToAds(context, GetRow(Tables.ColumnAds, row));
                    break;
                default:
                    return;
            }
            if (flag)
            {
                context.SaveChanges();
                PrintTable<T>(context);
            }
        }
        catch (DbUpdateException)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("\nДанные не были добавлены. Проверьте вводимые ID. " +
                "Если вы ссылаетесь на идентификаторы других таблиц, то эти записи должны существовать!");
            Console.ResetColor();
        }
    }

    /// <summary>
    /// Считывает данные для добавления в таблицы.
    /// </summary>
    /// <param name="TableName"></param>
    /// <param name="row"></param>
    /// <returns>Введенные данные списоком</returns>
    private static List<string> GetRow(List<string> TableName, List<string> row)
    {
            for (int i = 1; i < TableName.Count; i++)
            {
                Console.Write("Введите значение для столбца ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"'{TableName[i]}'");
                Console.ResetColor();
                var phrase = Console.ReadLine();
                while (string.IsNullOrEmpty(phrase))
                {
                    Console.WriteLine("Нельзя добавить пустую строку. Попробуйте ввести еще раз");
                    phrase = Console.ReadLine();
                }
                row.Add(phrase);
            }
            return row;
    }

    /// <summary>
    /// Добавляет строку в таблицу Регионы.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="row"></param>
    private static void AddRowToRegions(AvitoContext context, List<string> row)
    {
        var region = new Region
        {
            RegionNameEn = row[0],
            RegionNameRu = row[1]
        };
        context.Regions.Add(region);
    }

    /// <summary>
    /// Добавляет строку в таблицу Способы связи.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="row"></param>
    private static void AddRowToCommMethods(AvitoContext context, List<string> row)
    {
        var method = new CommMethod
        {
            Method = row[0]
        };
        context.CommMethods.Add(method);
    }

    /// <summary>
    /// Добавляет строку в таблицу Категории.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="row"></param>
    private static void AddRowToCategories(AvitoContext context, List<string> row)
    {
        var category = new Category
        {
            CategoryNameEn = row[0],
            CategoryNameRu = row[1]
        };
        context.Categories.Add(category);
    }

    /// <summary>
    /// Добавляет строку в таблицу пользователи.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="row"></param>
    /// <returns>Возвращает true в случае успешного добавления</returns>
    private static bool AddRowToUsers(AvitoContext context, List<string> row)
    {
        try
        {
            var user = new User
            {
                LastName = row[0],
                FirstName = row[1],
                MiddleName = row[2],
                Email = row[3],
                Phone = row[4],
                RegionId = Convert.ToInt32(row[5]),
                Rating = Convert.ToDouble(row[6])
            };
            context.Users.Add(user);
            return true;
        }
        catch(Exception)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("\nДанные не были добавлены. Проверьте формат вводимых данных" +
                "\n\tПример: Фомичева; Татьяна; Юрьевна; user1@example.com; 55555; 1; 1,8");
            Console.ResetColor();
            return false;
        }
    }

    /// <summary>
    /// Добавляет строку в таблицу Объявления.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="row"></param>
    /// <returns>Возвращает true в случае успешного добавления</returns>
    private static bool AddRowToAds(AvitoContext context, List<string> row)
    {
        try
        {
            var ad = new Ad
            {
                UserId = Convert.ToInt32(row[0]),
                CommMethodId = Convert.ToInt32(row[1]),
                CategoryId = Convert.ToInt32(row[2]),
                Text = row[3],
                Date = DateTime.Parse(row[4]).ToUniversalTime(),
                Price = Convert.ToDecimal(row[5]),
                Name = row[6]
            };
            context.Ads.Add(ad);
            return true;
        }
        catch (Exception)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("\nДанные не были добавлены. Проверьте формат вводимых данных" +
                "\n\tПример: 1; 1; 1; Текст объявления; 2022-01-02; 1600; Название объявления");
            Console.ResetColor();
            return false;
        }
    }
    #endregion
}
