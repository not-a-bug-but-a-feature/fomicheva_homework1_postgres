﻿global using homework1.Data;
global using homework1.Models;
global using System.ComponentModel.DataAnnotations;
global using ExtensionMethods;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.Extensions.Configuration;